function Stopwatch(){


	let duration = 0;
	let intervalId; 


	this.start = function() {
		//it is going to start the watch
		intervalId = setInterval(function(){
			duration++;
		}, (1000*60));
	}

	this.stop = function() {
		//it is going to stop the watch
		clearInterval(intervalId);
	}

	this.reset = function() {
		// it is going to reset duration

		duration = 0;
	}


	// it will assign duration property
	Object.defineProperty(this, "duration", {
		get: function() {
			return duration;
		}
	});


}